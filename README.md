# interview-prep
Practice interview problems & solutions

## References
- [Mastering Dynamic Programming - How to solve any interview problem (Part 1)](https://youtu.be/Hdr64lKQ3e4)
- [Change-making problem](https://wikipedia.org/wiki/Change-making_problem)
- [The Fibonacci sequence and linear algebra](https://fabiandablander.com/r/Fibonacci)
