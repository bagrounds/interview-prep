/* Given 2 run-length encoded vectors, return the dot product
Apparently this is a LeetCode premium problem: https://leetcode.com/problems/product-of-two-run-length-encoded-arrays
*/
export type Run = [number, number]
export type EncodedVector = Run[]
export type NonEmptyEncodedVector = [Run, ...EncodedVector]

const decompress = (v: EncodedVector): number[] =>
  v.flatMap(([value, length]) => Array(length).fill(value))

const dot = (a: number[], b: number[]): number => {
  if (a.length !== b.length) {
    throw new Error(`cannot compute dot product of different length vectors (a.length = ${a.length}, b.length=${b.length})`)
  }
  return a.reduce((sum, ai, i) => sum + ai * (b[i] as number), 0)
}

// decompress each vector, take dot product the easy way - O(N) in the size of the uncompressed vector
export const bruteForce = (a: EncodedVector, b: EncodedVector): number => {
  const inflatedA = decompress(a)
  const inflatedB = decompress(b)
  return dot(inflatedA, inflatedB)
}

// recursively multiply partial heads and tails of each vector - O(N) in the size of the compressed vector
export const recursive = (a: EncodedVector, b: EncodedVector): number => {
  const [a0, ...aTail] = a
  const [b0, ...bTail] = b
  if (!a0 && !b0) {
    return 0
  } else if (!b0) {
    throw new Error('cannot mulitply empty and non-empty vectors (b is empty)')
  } else if (!a0) {
    throw new Error('cannot mulitply empty and non-empty vectors (a is empty)')
  }
  const [a0Val, a0Len] = a0
  const [b0Val, b0Len] = b0
  if (a0Len < b0Len) {
    const bRemainder = b0Len - a0Len
    return a0Val * b0Val * a0Len + recursive(aTail, [[b0Val, bRemainder], ...bTail])
  } else if (b0Len < a0Len) {
    const aRemainder = a0Len - b0Len
    return b0Val * a0Val * b0Len + recursive([[a0Val, aRemainder], ...aTail], bTail)
  } else {
    return a0Val * b0Val * a0Len + recursive(aTail, bTail)
  }
}

// iteratively multiply each element, carefully tracking remainders for unequal length runs
export const iterative = (a: EncodedVector, b: EncodedVector): number => {
  const [a0] = a
  const [b0] = b
  if (!a0 && !b0) {
    return 0
  } else if (!b0) {
    throw new Error('cannot mulitply empty and non-empty vectors (b is empty)')
  } else if (!a0) {
    throw new Error('cannot mulitply empty and non-empty vectors (a is empty)')
  }

  let sum = 0
  for (let i = 0, j = 0, aR = 0, bR = 0; i < a.length && j < b.length;) {
    const currentA = a[i]
    if (!currentA) {
        throw new Error(`a[${i}] is undefined`)
    }
    const currentB = b[j]
    if (!currentB) {
        throw new Error(`b[${j}] is undefined`)
    }
    const A: Run = [currentA[0], aR || currentA[1]]
    const B: Run = [currentB[0], bR || currentB[1]]
    const [aVal, aLen] = A
    const [bVal, bLen] = B
    if (aLen < bLen) {
      sum += aVal * bVal * aLen
      aR = 0
      bR = bLen - aLen
      i++
    } else if (bLen < aLen) {
      sum += aVal * bVal * bLen
      aR = aLen - bLen
      bR = 0
      j++
    } else {
      sum += aVal * bVal * aLen
      aR = 0
      bR = 0
      i++
      j++
    }
  }
  return sum
}
