import * as RunLengthEncodedDotProduct from './run-length-encoded-dot-product'
import type { EncodedVector } from './run-length-encoded-dot-product'

type Test = { a: EncodedVector; b: EncodedVector; r: number }

const bruteForceExamples: Omit<Test, 'r'>[] = [
  { a: [[2, 2], [3, 3]], b: [[3, 1], [9, 4]] },
  { a: [[2, 2], [3, 3]], b: [[3, 1], [9, 2], [17, 2]] },
  { a: [[2, 2], [3, 1], [31, 2]], b: [[3, 1], [9, 4]] },
]
const bruteForcedResults = bruteForceExamples
  .map(({ a, b }) => ({ a, b, r: RunLengthEncodedDotProduct.bruteForce(a, b) }))

const tests: Test[] = [
  { a: [], b: [], r: 0 },
  { a: [[2, 2]], b: [[3, 2]], r: 12 },
  { a: [[2, 2], [3, 1]], b: [[3, 1], [7, 2]], r: 6 + 14 + 21 },
  ...bruteForcedResults
]

tests.forEach(({ a, b, r}, i) => {
  const actualRecursive = RunLengthEncodedDotProduct.recursive(a, b)
  const actualIterative = RunLengthEncodedDotProduct.iterative(a, b)
  if (actualRecursive !== r) {
    throw new Error(`[test ${i}] recursive(${a}, ${b}) = ${actualRecursive}; expected ${r}`)
  } else if (actualIterative !== r) {
    throw new Error(`[test ${i}] iterative(${a}, ${b}) = ${actualIterative}; expected ${r}`)
  } else {
    console.log(`[test ${i}] Success ${JSON.stringify(a)} * ${JSON.stringify(b)} = ${JSON.stringify(r)}`)
  }
})

