import Decimal from 'decimal.js'
import * as Fib from './fibonacci'
import * as Change from './change-making'

// Helpers
export const range = (from: number, to: number, step: number = 1): number[] => {
  const length = Math.ceil((to - from + 1) / step)
  const delta = from <= to ? step : -step
  return Array
    // @ts-expect-error Argument of type '{ length: number; }' is not assignable to parameter of type 'unknown[]'. Type '{ length: number; }' is missing the following properties from type 'unknown[]': pop, push, concat, join, and 25 more.ts(2345)
    .apply(null, { length })
    .map((_x, i) => from + i * delta)
}

const unfold = <A, B>(next: (b: B) => [A, B], stop: (b: B) => boolean, seed: B): A[] => {
  const results = []
  for (let i = 0, s = seed; !stop(s); i++) {
    const [v, n] = next(s)
    console.debug(`unfold(i=${i},s=${s},v=${v},n=${n})`)
    results[i] = v
    s = n
  }
  return results
}

const exp = (e: number) => (n: number): number[] => unfold((i: number) => [e**i, i+1], i => i > n, 0)
const exp2 = exp(2)

type FormatInput<R> = { name: string; result: R; duration: number }
const format = <R>({name, result, duration}: FormatInput<R>): string =>
  `${duration.toFixed(3).padStart(8)} ms| ${name} = ${result}`

type TimeResult = { result: string; duration: number }
const time = <T extends number | bigint | Decimal>(f: () => T): TimeResult => {
  const t1 = performance.now()
  let result
  try {
    result = f()
    result = `${result}`.length > 7
      ? (result instanceof Decimal
        ? result.toExponential(3)
        : result.toLocaleString('en-US', { notation: 'scientific' }))
      : `${result}`
  } catch (error) {
    result = error instanceof Error ? error.message : `${error}`
  }
  const t2 = performance.now()
  const duration = t2 - t1
  return { result, duration }
}

const formatLog = <R>(x: FormatInput<R>): void => console.log(format(x))

const examples = [
  {
    module: 'Fibonacci',
    f: Fib.naiveRecursive,
    inputs: exp2(5).map(x => [x] as [number])
  },
  {
    module: 'Fibonacci',
    f: Fib.memoizedRecursive,
    inputs: exp2(13).map(x => [x] as [number])
  },
  {
    module: 'Fibonacci',
    f: Fib.iterative,
    inputs: exp2(17).map(x => [x] as [number])
  },
  {
    module: 'Fibonacci',
    f: Fib.closed,
    inputs: exp2(56).map(x => [x] as [number])
  },
  {
    module: 'Change-Making',
    f: Change.naiveRecursive,
    inputs: exp2(5).map(x => [x, [1, 5, 10, 25]] as [number, number[]])
  },
  {
    module: 'Change-Making',
    f: Change.memoizedRecursive,
    inputs: exp2(12).map(x => [x, [1, 5, 10, 25]] as [number, number[]])
  },
  {
    module: 'Change-Making',
    f: Change.iterative,
    inputs: exp2(22).map(x => [x, [1, 4, 5]] as [number, number[]])
  },
  {
    module: 'Change-Making',
    f: Change.iterativeBoundedCache,
    inputs: exp2(22).map(x => [x, [1, 4, 5]] as [number, number[]])
  },
] as const

const main = () => {
  console.log({ examples })
  examples
    .forEach(({ module, f, inputs }) => {
      const divider = '='.repeat(11)
      console.log(`${divider}| ${module}.${f.name}`)
      inputs
        .forEach(i => {
          // @ts-expect-error
          const { result, duration } = time(() => f(...i))
          formatLog({ result, duration, name: `${module}.${f.name}(${i.join(', ')})` })
        })
    })
}

main()
