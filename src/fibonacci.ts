import Decimal from 'decimal.js'

const fibGuard = (n: number | bigint): void => {
  if (n <= 0) {
    throw new Error(`fib is not defined for values less than 1 (n=${n})`)
  }
}

export const naiveRecursive = (n: number): number => {
  fibGuard(n)
  return n === 1
    ? 1
    : n === 2
    ? 1
    : naiveRecursive(n - 1) + naiveRecursive(n - 2)
}

export const memoizedRecursive = (n: number, cache: number[] = []): number => {
  fibGuard(n)
  const cached = cache[n]
  if (typeof cached === 'number') {
    return cached
  }
  const result = (n === 1 || n === 2)
    ? 1
    : memoizedRecursive(n - 1, cache) + memoizedRecursive(n - 2, cache)
  cache[n] = result
  return result
}

export const iterative = (n: number): bigint => {
  fibGuard(n)
  if (n < 3) return 1n
  let fib_n_minus_2 = 1n
  let fib_n_minus_1 = 1n
  let fib_n = 2n
  for (let i = 3; i <= n; i++) {
    fib_n = fib_n_minus_2 + fib_n_minus_1
    fib_n_minus_2 = fib_n_minus_1
    fib_n_minus_1 = fib_n
  }
  return fib_n
}

// https://fabiandablander.com/r/Fibonacci
export const closed = (n: number): Decimal => {
  const d_sqrt_5 = Decimal.sqrt(5)
  return d_sqrt_5.plus(1).div(2).pow(n).dividedBy(d_sqrt_5).round()
}
