// https://en.wikipedia.org/wiki/Change-making_problem
const changeGuard = (target: number, coins: number[]): void => {
  if (target < 0) {
    throw new Error(`change is not defined for values less than 0 (target=${target})`)
  }
  if (!coins.length) {
    throw new Error(`change is not defined without any coins (coinds=${coins})`)
  }
}

export const naiveRecursive = (target: number, coins: number[]): number => {
  changeGuard(target, coins)
  if (target === 0) {
    return 0
  }
  if (coins.includes(target)) {
    return 1
  }
  const solutions = coins
    .map(coin => target - coin)
    .filter(target => target > 0)
    .map(target => naiveRecursive(target, coins) + 1)
  return Math.min(...solutions)
}

export const memoizedRecursive = (target: number, coins: number[], cache: number[] = []): number => {
  changeGuard(target, coins)
  if (target === 0) {
    return 0
  }
  if (coins.includes(target)) {
    return 1
  }
  const cached = cache[target]
  if (cached) {
    return cached
  }
  const solutions = coins
    .map(coin => target - coin)
    .filter(target => target > 0)
    .map(target => memoizedRecursive(target, coins, cache) + 1)
  const best = Math.min(...solutions)
  cache[target] = best
  return best
}

export const iterative = (target: number, coins: number[]): number => {
  changeGuard(target, coins)
  const cache: number[] = [0]
  for (let i = 1; i <= target; i++) {
    if (coins.includes(i)) {
      cache[i] = 1
    }
    const solutions = coins
      .map(coin => i - coin)
      .filter(target => target >= 0)
      .map(target => {
        const hit = cache[target] 
        // @ts-expect-error
        return hit + 1
      })
    const best = Math.min(...solutions)
    cache[i] = best
  }
  // @ts-expect-error
  return cache[target]
}

export const iterativeBoundedCache = (target: number, coins: number[]): number => {
  changeGuard(target, coins)
  const maxCoin = Math.max(...coins)
  const cache: number[] = []
  for (let i = 1; i <= target; i++) {
    const solutions = coins
      .map(coin => i - coin)
      .filter(target => target >= 0)
      .map(target => {
        const hit = target === 0 ? 0 : cache[target % maxCoin]
        // @ts-expect-error
        return hit + 1
      })
    const best = Math.min(...solutions)
    cache[i % maxCoin] = best
  }

  const result = cache[target % maxCoin]
  // @ts-expect-error
  return result
}